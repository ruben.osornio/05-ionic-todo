import { Injectable } from '@angular/core';
import { Lista } from '../models/lista.model';

@Injectable({
  providedIn: 'root'
})
export class DeseosService {

  public listas: Lista[] = [];

  constructor() { 
    this.cargarStorage();
    console.log(this.listas);
  }

  crearLista(titulo: string){
    const lista1 = new Lista(titulo);
    this.listas.push(lista1);
    this.guardarStorage();

    return lista1.id;
  }

  borrarLista(lista: Lista){
    this.listas = this.listas.filter(listaData =>{
      return listaData.id != lista.id;
    });

    this.guardarStorage();
  }

  obtenerLista(id: string | number){
    id = Number(id);
    return this.listas.find( listaData =>{
      return listaData.id === id;
    })
  }

  guardarStorage(){
    localStorage.setItem('data', JSON.stringify(this.listas));
  }

  cargarStorage(){

    if(localStorage.getItem('data')){
      this.listas = JSON.parse(localStorage.getItem('data'));
    }else{
      this.listas = [];
    }
  }

}
